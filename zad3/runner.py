import os

import numpy as np
import torch
from PIL import Image
from torch.optim import Adam
from torch.utils.data import TensorDataset, random_split, DataLoader
from torchvision import transforms

from model import DNModel
from rmse import RMSELoss

train_dir_clean = 'data/clean'
train_dir_distorted = 'data/distorted'
test_dir = 'data/test_distorted'

torch.manual_seed(0)
np.random.seed(0)


class DNRunner:

    def __init__(self, batch_size=64, eval_split=0.2):
        self.batch_size = batch_size
        self.eval_split = eval_split

        self.train_loader, self.eval_loader, self.test_loader = self._prepare_datasets()

        self.model = DNModel()
        self.loss = RMSELoss()
        self.optimizer = Adam(self.model.parameters(), lr=0.0003, amsgrad=True, weight_decay=0.01)

    def train(self, epochs=3):
        print('training... ', flush=True)
        scores = []
        for i_epoch in range(epochs):
            self.model.train()
            for i, (x, y) in enumerate(self.train_loader):
                self.optimizer.zero_grad()
                output = self.model(x)
                # result = output
                result = torch.clamp(x - output, 0., 1.)
                loss = self.loss(result, y)
                loss.backward()
                self.optimizer.step()

                if i % 50 == 0:
                    print(f'\tEpoch {i_epoch} iter {i}/\
{len(self.train_loader.dataset) // self.batch_size} loss: {loss.item()}', flush=True)

            self.model.eval()
            scores = []
            for i, (x, y) in enumerate(self.eval_loader):
                output = self.model(x)
                # result = output
                result = torch.clamp(x - output, 0., 1.)
                score = self.loss(result, y)
                scores.append(score.item())
            print(f"\tEval score: ", sum(scores) / len(scores), flush=True)
        print('done')

    def test(self):
        print('testing... ', flush=True, end='')
        self.model.eval()
        results = []
        for (x,) in self.test_loader:
            output = self.model(x)
            # result = output
            result = torch.clamp(x - output, 0., 1.)
            results.append(result.detach().numpy())
        results = np.concatenate(results)
        DNRunner.save_result(results)

    def _load_data(self, data_type):
        print(f'\tloading {data_type} data... ', flush=True, end='')
        assert data_type in ['train', 'test']
        data = targets = []
        trans = transforms.Compose([transforms.ToTensor()])
        if data_type == 'train':
            train_files = sorted([file_name for file_name in os.listdir(train_dir_clean)
                                  if file_name.endswith('.jpg')], key=lambda x: int(x.split('.')[0]))
            for file_name in train_files:
                img_distorted = Image.open(os.path.join(train_dir_distorted, file_name))
                img_clean = Image.open(os.path.join(train_dir_clean, file_name))
                data.append(trans(img_distorted))
                targets.append(trans(img_clean))
            targets = torch.stack(targets)
        else:
            test_files = sorted([file_name for file_name in os.listdir(test_dir)
                                 if file_name.endswith('.jpg')], key=lambda x: int(x.split('.')[0]))
            for file_name in test_files:
                img_distorted = Image.open(os.path.join(test_dir, file_name))
                data.append(trans(img_distorted))
        data = torch.stack(data)
        print('done')
        return (data, targets) if data_type == 'train' else data

    def _prepare_datasets(self):
        print('preparing datasets... ', flush=True)
        train_x, train_y = self._load_data('train')
        train_dataset = TensorDataset(train_x, train_y)
        train_len = len(train_dataset)
        train_dataset, eval_dataset = random_split(train_dataset,
                                                   [train_len - int(train_len * self.eval_split),
                                                    int(train_len * self.eval_split)])

        test_x = self._load_data('test')
        test_dataset = TensorDataset(test_x)

        train_loader = DataLoader(train_dataset, batch_size=self.batch_size)
        eval_loader = DataLoader(eval_dataset, batch_size=self.batch_size)
        test_loader = DataLoader(test_dataset, batch_size=self.batch_size)
        print('done')
        return train_loader, eval_loader, test_loader

    @staticmethod
    def save_result(images: np.ndarray, out_path: str = 'submission.csv'):
        print('saving results... ', flush=True, end='')
        assert images.shape == (400, 3, 48, 48)
        flat_img = images.reshape(400, -1)
        n_rows = np.prod(images.shape)
        y_with_id = np.concatenate([np.arange(n_rows).reshape(-1, 1), flat_img.reshape(n_rows, 1)], axis=1)
        np.savetxt(out_path, y_with_id, delimiter=",", fmt=['%d', '%.4f'], header="id,expetced", comments='')
        print('done')
