from runner import DNRunner


def main():
    print('START')
    runner = DNRunner()
    runner.train(10)

    runner.test()
    print("DONE")


if __name__ == "__main__":
    main()
