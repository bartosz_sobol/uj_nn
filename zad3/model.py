import torch.nn as nn


class DNModel(nn.Module):
    def __init__(self):
        super(DNModel, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(3, 8, kernel_size=3, padding=1),
            nn.Conv2d(8, 8, kernel_size=3, padding=1),
            nn.Conv2d(8, 3, kernel_size=3, padding=1),
        )

    def forward(self, x):
        return self.conv(x)
