from mlp_runner import MLPRunner


def main():
    print('START')
    runner = MLPRunner(342, 10, 256)
    runner.train(20)
    runner.test()
    print("DONE")


if __name__ == "__main__":
    main()
