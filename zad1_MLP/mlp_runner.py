import os
import numpy as np
from typing import Sequence

import torch
from sklearn import preprocessing
from torch import nn
from torch.optim import Adam
from torch.utils.data import TensorDataset, DataLoader, random_split

from mlp_model import MLPModel

torch.manual_seed(0)
np.random.seed(0)

data_dir = 'data'


class MLPRunner:
    def __init__(self, hidden_size, in_dim, out_dim, batch_size=64, lr=0.001, dropout_p=0.5):
        self.hidden_size = hidden_size
        self.in_dim = in_dim
        self.out_dim = out_dim
        self.batch_size = batch_size
        self.dropout_p = dropout_p

        self.model = MLPModel(self.hidden_size, self.in_dim, self.out_dim, self.dropout_p)

        self.loss = nn.CrossEntropyLoss()
        self.optimizer = Adam(self.model.parameters(), lr=lr, amsgrad=True, weight_decay=0)

        train_data, train_labels = MLPRunner.load_data('train')
        test_data = MLPRunner.load_data('test')

        train_data = MLPRunner.preprocess_data(train_data)
        test_data = MLPRunner.preprocess_data(test_data)

        train_labels = torch.from_numpy(train_labels)

        self.train_data = TensorDataset(train_data, train_labels)
        train_len = len(self.train_data)
        self.train_data, self.eval_data = random_split(self.train_data, [train_len - train_len // 5, train_len // 5])
        self.test_data = TensorDataset(test_data)

        self.train_loader = DataLoader(self.train_data, batch_size=self.batch_size)
        self.eval_loader = DataLoader(self.eval_data, batch_size=self.batch_size)
        self.test_loader = DataLoader(self.test_data, batch_size=self.batch_size)

    def train(self, epochs=3):
        print('training...', end='', flush=True)
        for i_epoch in range(epochs):
            self.model.train()
            for i, (data, labels) in enumerate(self.train_loader):
                # print(data)
                # print(labels)
                self.optimizer.zero_grad()
                output = self.model(data)
                loss = self.loss(output, labels)
                loss.backward()
                self.optimizer.step()

                if (i + 1) % 100 == 0:
                    print(f'Epoch {i_epoch} iter {i + 1}/{len(self.train_data) // self.batch_size} loss: {loss.item()}')

            self.model.eval()
            with torch.no_grad():
                correct = 0
                for i, (data, labels) in enumerate(self.eval_loader):
                    output = self.model(data)
                    result = torch.argmax(output, dim=1)
                    good = torch.eq(result, labels)
                    correct += torch.sum(good).item()
                    loss = self.loss(output, labels)
                    if (i + 1) % 100 == 0:
                        print(f'loss: {loss.item()}')
                print(f"Eval accuracy: {correct / len(self.eval_data)}")

        print('done')

    def test(self):
        print('test run...', end='', flush=True)
        results = []
        self.model.eval()
        with torch.no_grad():
            for i, (data,) in enumerate(self.test_loader):
                output = self.model(data)
                result = torch.argmax(output, dim=1)
                results.append(result)
        print('done')
        results = torch.cat(tuple(results)).numpy()
        MLPRunner.save_prediction(results)

    @staticmethod
    def preprocess_data(data):
        data = torch.from_numpy(data)
        means = data.mean(dim=1, keepdim=True)
        stds = data.std(dim=1, keepdim=True)
        data = (data - means) / stds
        return data

    @staticmethod
    def load_data(data_type):
        assert data_type in ['train', 'test']
        print(f'loading data {data_type}...', end='', flush=True)

        if data_type == 'train':
            data = np.loadtxt(fname=os.path.join(data_dir, 'train_data.csv'),
                              delimiter=',', skiprows=1, dtype=np.float32)
            labels = np.loadtxt(fname=os.path.join(data_dir, 'train_labels.csv'),
                                delimiter=',', skiprows=1, dtype=np.int64)
            print('done')
            return data, labels
        elif data_type == 'test':
            data = np.loadtxt(fname=os.path.join(data_dir, 'test_data.csv'),
                              delimiter=',', skiprows=1, dtype=np.float32)
            print('done')
            return data

    @staticmethod
    def save_prediction(prediction: Sequence[int], file_name: str = 'submission.csv'):
        print('saving reslts...', end='', flush=True)
        pred_with_id = np.stack([np.arange(len(prediction)), prediction], axis=1)
        np.savetxt(fname=file_name, X=pred_with_id, fmt='%d', delimiter=',', header='id,label', comments='')
        print('done')
