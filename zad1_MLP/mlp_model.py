import torch
import torch.nn as nn


class MLPModel(nn.Module):

    def __init__(self, in_dim, hidden_size, out_dim, drooup_p):
        super(MLPModel, self).__init__()
        slope = 0.1
        self.out = nn.Sequential(
            nn.Linear(in_dim, 768),
            nn.LeakyReLU(negative_slope=slope),
            nn.BatchNorm1d(num_features=768),
            nn.Dropout(drooup_p),
            nn.Linear(768, 768),
            nn.LeakyReLU(negative_slope=slope),
            nn.BatchNorm1d(num_features=768),
            nn.Dropout(drooup_p),
            nn.Linear(768, 768),
            nn.LeakyReLU(negative_slope=slope),
            nn.BatchNorm1d(num_features=768),
            nn.Dropout(drooup_p),
            nn.Linear(768, out_dim),

        )
        self.out.apply(MLPModel.init_weights)

    def forward(self, x):
        return self.out(x)

    @staticmethod
    def init_weights(layer):
        if type(layer) == nn.Linear:
            torch.nn.init.xavier_normal_(layer.weight)
            # layer.bias.data.fill_(0.01)
