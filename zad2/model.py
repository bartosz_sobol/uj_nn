import torch
import torch.nn as nn


class Detector(torch.nn.Module):
    def __init__(self):
        super(Detector, self).__init__()
        slope = 0.1

        self.conv = nn.Sequential(
            nn.Conv2d(1, 64, kernel_size=3),
            nn.LeakyReLU(negative_slope=slope),
            nn.BatchNorm2d(64),
            nn.MaxPool2d(kernel_size=2),

            nn.Conv2d(64, 64, kernel_size=3),
            nn.LeakyReLU(negative_slope=slope),
            nn.BatchNorm2d(64),
            nn.MaxPool2d(kernel_size=2),

            nn.Conv2d(64, 64, kernel_size=3),
            nn.LeakyReLU(negative_slope=slope),
            nn.BatchNorm2d(64),
            nn.MaxPool2d(kernel_size=2),
        )

        self.lin = nn.Sequential(
            nn.Linear(2880, 2048),
            nn.LeakyReLU(negative_slope=slope),
            nn.BatchNorm1d(2048),
            nn.Dropout(0.5),

            nn.Linear(2048, 1024),
            nn.LeakyReLU(negative_slope=slope),
            nn.BatchNorm1d(1024),
            nn.Dropout(0.2),

            nn.Linear(1024, 2),
        )

    def forward(self, x):
        result = x

        result = self.conv(result)

        result = result.view(result.size(0), -1)

        result = self.lin(result)

        result = nn.functional.softmax(result, dim=1)

        return result
