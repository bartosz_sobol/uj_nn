from runner import Runner


def main():
    print('START')
    runner = Runner(data_from_save=True)
    runner.train(13)
    runner.save_state_dict()
    runner.test()
    print("DONE")


if __name__ == "__main__":
    main()
