import os

import librosa
import numpy as np
import torch
from sklearn.metrics import roc_auc_score
from torch.optim import Adam
from torch.utils.data import TensorDataset, random_split, DataLoader

from model import Detector

torch.manual_seed(0)
np.random.seed(0)

train_dir = 'data/train'
test_dir = 'data/test'
tmp_dir = 'data/tmp_wrapped'


class Runner:
    def __init__(self, batch_size=64, eval_split=0.2, data_from_save=False):
        self.batch_size = batch_size

        self.x_train, self.y_train, self.x_test = self.load_data(data_from_save)

        self.model = Detector()
        self.loss = torch.nn.CrossEntropyLoss()
        self.optimizer = Adam(self.model.parameters(), lr=0.0001, amsgrad=True, weight_decay=0)

        self.train_loader, self.eval_loader, self.test_loader = self.prepare_datasets(eval_split)

    def load_data(self, from_save=False):
        print('loading data...', flush=True)
        if not from_save:
            print('\tfrom files...', end='', flush=True)
            x_train, y_train = Runner.load_train()
            x_test = Runner.load_test()
            np.save(os.path.join(tmp_dir, 'x_train_tmp'), x_train)
            np.save(os.path.join(tmp_dir, 'y_train_tmp'), y_train)
            np.save(os.path.join(tmp_dir, 'x_test_tmp'), x_test)
        else:
            print('\tfrom save...', end='', flush=True)
            x_train = np.load(os.path.join(tmp_dir, 'x_train_tmp.npy'))
            y_train = np.load(os.path.join(tmp_dir, 'y_train_tmp.npy'))
            x_test = np.load(os.path.join(tmp_dir, 'x_test_tmp.npy'))
        print('done')
        return x_train, y_train, x_test

    def prepare_datasets(self, eval_split):
        print('preparing datasets...', end='', flush=True)
        if self.x_train is None or self.y_train is None or self.x_test is None:
            raise Exception('No data loaded')
        train_dataset = TensorDataset(torch.Tensor(self.x_train), torch.LongTensor(self.y_train))
        train_len = len(train_dataset)
        train_dataset, eval_dataset = random_split(train_dataset,
                                                   [train_len - int(train_len * eval_split),
                                                    int(train_len * eval_split)])

        test_dataset = TensorDataset(torch.Tensor(self.x_test))

        train_loader = DataLoader(train_dataset, batch_size=self.batch_size)
        eval_loader = DataLoader(eval_dataset, batch_size=self.batch_size)
        test_loader = DataLoader(test_dataset, batch_size=self.batch_size)
        print('done')
        return train_loader, eval_loader, test_loader

    def train(self, epochs=10):
        print('training...', flush=True)
        best_predictions, best_score = None, 0.
        losses, scores = [], []
        for i_epoch in range(epochs):
            running_loss = 0
            self.model.train()
            for i, (x, y) in enumerate(self.train_loader):
                self.optimizer.zero_grad()
                output = self.model(x)
                loss = self.loss(output, y)
                loss.backward()
                self.optimizer.step()
                running_loss += loss.item()

                if 1 % 10 == 0:
                    print(f'Epoch {i_epoch} iter {i}/\
{len(self.train_loader.dataset) // self.batch_size} loss: {loss.item()}')
            losses.append(running_loss)

            self.model.eval()
            predictions = []
            for i, (x, y) in enumerate(self.eval_loader):
                output = self.model(x)
                predictions.append(output[:, 1].detach().numpy())
            predictions = np.concatenate(predictions, axis=0)
            # Metryką testującą jest ROC AUC
            score = roc_auc_score(self.eval_loader.dataset[:][1].numpy(), predictions)
            scores.append(score)
            print(f"Eval score: ", score)
            if score > best_score:
                best_score = score
                best_predictions = predictions
                # np.save('tmp_preds', best_predictions)
                # torch.save(clf.state_dict(), 'tmp_model.pt')
        print('done')

    def test(self):
        print('test run...', end='', flush=True)
        self.model.eval()
        results = []
        for batch in self.test_loader:
            out = self.model(batch[0])
            results.append(out[:, 1].detach().numpy())
        print('done')
        self.save_predictions(np.concatenate(results, axis=0))

    def save_predictions(self, predictions):
        """Zapisuje predykcje do pliku zgodnego z formatem odpowiedzi.

          Args:
              predictions (list): Lista predykcji (prawdopodobieństw).
          """
        print('saving reslts...', end='', flush=True)
        with open('data/sampleSubmission.csv', 'r') as file:
            submission_text = file.read().split()
            header = submission_text[0]
            lines = submission_text[1:]

        output_lines = [header]
        for pred, line in zip(predictions, lines):
            output_lines.append("{},{}".format(line.split(',')[0], pred))

        with open('submission.csv', 'w') as file:
            file.write('\n'.join(output_lines) + '\n')
        print('done')

    @staticmethod
    def load_mel_spectrogram(file_name, start=0, stop=None, n_mels=60):
        """czytuje mel spektrogram z pliku.

        Args:
            file_name (str): Nazwa pliku z nagraniem.
            start (float): Sekunda, w której zaczyna się interesujący fragment.
            stop (float): Sekunda, w której kończy się interesujący fragment.
            n_mels (int): Liczba meli na spektrogramie (wysokość spektrogramu).

        Returns:
            ndarray: Spektrogram.
        """
        samples, sample_rate = librosa.core.load(file_name, sr=None)
        # samples = sp.signal.medfilt(samples, kernel_size=3)
        samples = samples[int(start * sample_rate):int(stop * sample_rate) if stop else None]
        spectrogram = librosa.feature.melspectrogram(y=samples, sr=sample_rate,
                                                     n_mels=n_mels, fmin=4000, fmax=9500)
        return spectrogram

    @staticmethod
    def read_labels():
        """Wczytuje etykiety czasowe z pliku labels.txt w folderze train.

        Returns:
            ndarray: Tablica z etykietami czasowymi zawierająca kolumny: nr nagrania, sekunda początku dźwięku, sekunda końca dźwięku.
        """
        labels = []
        with open(os.path.join(train_dir, 'labels.txt'), 'r') as file:
            text = file.read()
            for line in text.split('\n')[1:]:
                if len(line) > 1:
                    rec, start, stop = line.split(',')
                    rec, start, stop = int(rec[3:]), float(start), float(stop)
                    labels.append([rec, start, stop])
        return np.array(labels)

    @staticmethod
    def check_voices(second, labels, tol=0.):
        """Sprawdza czy w ramce czasowej [second, second+1] znajduje się głos według etykiet `labels`.

        Args:
            second (float): Sekunda nagrania.
            labels (ndarray): Tablica z etykietami, której 2 kolumna oznacza początek, a 3-cia - koniec nagrania.
            tol (float): Tolerancja na brzegach fragmentu. Dźwięk, żeby był uznany, musi się kończyć po czasie `second+tol`
                lub zaczynać przed czasem `second+1-tol`.
        Returns:
            bool: Czy w ramce czasowej jest odgłos ptaka.
        """
        return (second <= labels[1] < second + 1 - tol) or \
               (second + 1 > labels[2] > second + tol) or \
               (labels[1] < second and labels[2] > second + 1)

    @staticmethod
    def map_seconds_to_y(labels):
        """Tworzy etykiety dla każdej kolejnej sekundy 10-sekundowego nagrania. -1 oznacza niepewną etykietę (urwane dźwięki na brzegach).

        Args:
            labels (ndarray): Tablica z etykietami, której 2 kolumna oznacza początek, a 3-cia - koniec nagrania.
        Returns:
            ndarray: Tablica z binarnymi etykietami dla każdej z 10 sekund z możliwą niepewną etkietą -1.
        """
        y = [0] * 10
        y_restrictive = [0] * 10
        for s in range(10):
            for l in labels:
                if Runner.check_voices(s, l):
                    y[s] = 1
                if Runner.check_voices(s, l, 0.02):
                    y_restrictive[s] = 1
            if y[s] != y_restrictive[s]:
                y[s] = -1
        return y

    @staticmethod
    def load_train():
        """Wczytuje dane treningowe.

        Returns:
            (ndarray, ndarray): Tablica z danymi treningowymi, tablica z binarnymi etykietami treningowymi.
        """
        labels = Runner.read_labels()
        x_train, y_train = [], []
        train_files = sorted([file_name for file_name in os.listdir(train_dir)
                              if file_name.endswith('.wav')], key=lambda x: int(x.split('.')[0][3:]))
        for file_name in train_files:
            recording_id = int(file_name.split('.')[0][3:])
            recording_labels = labels[labels[:, 0] == recording_id]
            y_binary = Runner.map_seconds_to_y(recording_labels)
            for i, y in enumerate(y_binary):
                if y != -1:
                    try:
                        representation = Runner.load_mel_spectrogram(os.path.join(train_dir, file_name), start=i,
                                                                     stop=i + 1)
                        x_train.append([representation])
                        y_train.append(y)
                    except ValueError:
                        print('Error reading file', file_name)
                    except TypeError:
                        print('Unsupported type', file_name)
        return np.array(x_train), np.array(y_train)

    @staticmethod
    def load_test():
        """Wczytuje dane testowe.

        Returns:
            ndarray: Tablica z danymi testowymi.
        """
        with open('data/sampleSubmission.csv', 'r') as file:
            lines = file.read().split()[1:]
            sample_ids = [line.split(',')[0] for line in lines]
            samples = np.array([s.split('/') for s in sample_ids])

        x_test = []
        test_files = sorted([file_name for file_name in os.listdir(test_dir)
                             if file_name.endswith('.wav')], key=lambda x: int(x.split('.')[0][3:]))
        for file_name in test_files:
            recording_id = file_name.split('.')[0][3:]
            time_markers = samples[samples[:, 0] == recording_id, 1].astype(np.int)
            for t in time_markers:
                representation = Runner.load_mel_spectrogram(os.path.join(test_dir, file_name), start=t, stop=t + 1)
                x_test.append([representation])
        return np.array(x_test)

    def save_state_dict(self, path='saved_weights'):
        print('saving model state...', flush=True, end='')
        torch.save(self.model.state_dict(), path)
        print('done')

    def load_state_dict(self, path='saved_weights'):
        print('loading model state...', flush=True, end='')
        self.model.load_state_dict(torch.load(path))
        print('done')
